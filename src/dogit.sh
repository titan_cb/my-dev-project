#!/usr/bin/env bash

# import library functions
. lib/init.sh
. lib/cli.sh

if [ ! -z ${DEBUG+x} ]; then
  log "debug mode is ON"
else
  log "debug mode is OFF"
fi

usage

for dir in ${1}/*/
do
	echo "${dir}"
done	

