
cred="\e[01;31m"
cgreen="\e[01;32m"
cyellow="\e[01;33m"
cblue="\e[01;34m"
cpurple="\e[1;35m"
cinfo=${cblue}
cend="\e[00m"

usage () {
	execName=`basename "$0"`

	builtin echo ""
	builtin echo "usage:  ${execName} <directory>"
	builtin echo ""
	builtin echo ""
	builtin echo " <directory>    iterate over all subdirectories of <directory>"
	builtin echo ""
}

echo () {
	prefix=$(date +"[%F %T]")
	builtin echo -e "${prefix} ${1}"
}


log () {
	prefix=$(date +"[%F %T]")
	builtin echo -e "${prefix} I ${cblue}${1}${cend}"
}

logQ () {
	prefix=$(date +"[%F %T]")
	builtin echo -e "${prefix} Q ${cgreen}${1}${cend}"
}

logE () {
	prefix=$(date +"[%F %T]")
	builtin echo -e "${prefix} E ${cred}${1}${cend}"
}

logW () {
	prefix=$(date +"[%F %T]")
i	builtin echo -e "${prefix} W ${cpurple}${1}${cend}"
}

debug () {
	if [ ! -z ${DEBUG+x} ]
	then
		prefix=$(date +"[%F %T]")
		builtin echo -e "${prefix} ${cyellow}${1}${cend}"
	fi
}


